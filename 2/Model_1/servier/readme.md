# Servier

Servier is a package that provides functionality for training, evaluating, and predicting properties of chemical compounds using deep neural networks. It includes modules for data preprocessing, model training, feature extraction, and result visualization.

## Installation

Please download the package by: git clone https://gitlab.com/saeed.khazaee/serviertask.git

To install the Servier package, you can use "pip":

Go to the directory of the project where you download the package and run "pip install ."

## Usage

After installing the package, you can use the "servier" command-line interface to perform various tasks.

### Training

To train the model, use the "train" command:

servier train --dataset_path dataset.csv --model_path dnn.h5 --autoencoder_path en.h5

### Evaluation

To evaluate the model, use the "evaluate" command:

servier evaluate --dataset_path dataset.csv --model_path dnn.h5 --autoencoder_path en.h5

### Prediction

To make predictions for a given smile, use the "predict" command:

servier predict --smile smile_example --model_path dnn.h5 --autoencoder_path en.h5

where:

- "dataset.csv": Path to the dataset file in CSV format.
- "dnn.h5": Path to save the trained model in HDF5 format.
- "en.h5": Path to save the trained autoencoder in HDF5 format.
- "smile_example": The smile string for which to make predictions.

## Flask API

The package also provides a Flask API for making predictions. To run the Flask server, execute the `using_flask.py` file:

command:
python using_flask.py

After the server is running, you can make prediction requests to the API. An example of how to make such requests can be found in the API_request_example.py file.

command:
python API_request_example.py

Note: Make sure the Flask server is running when you run API_request_example.py.

## Docker Usage
<<<<<<< HEAD
=======
You can also run the application in a Docker container. To do this, first build the Docker image from the Dockerfile:
>>>>>>> 9f56777d88be66a417cf5987b2b556f4d073e169

To run the application in a Docker container, please follow these steps:

### Building the Docker Image

1. Make sure you have Docker installed on your machine.
2. Navigate to the project directory containing the Dockerfile.
3. Build the Docker image using the following command:

command: 
   docker build -t servier .
### Running the Docker Container
Once the Docker image is built, you can run the container with the following command:


docker run -p 5000:5000 -v /absolute path:/app servier 

Replace /path with the actual path to the directory containing your dataset file and models.

### Handling the Dataset and Models
Please note that the Docker image does not include the dataset file and models by default. You need to mount the directory containing these files as a volume in the Docker container.



Place the dataset file (dataset_single.csv) and the required models (e.g., dnn.h5, en.h5) inside the app directory.

When running the Docker container, use the -v flag to mount the app directory:


docker run -p 5000:5000 -v /absolute path:/app servier
Replace /path with the actual path to the app directory on your host system.

By following these steps, the dataset file and models will be accessible inside the Docker container at the /path directory, allowing the application to utilize them.

<<<<<<< HEAD
=======
command:
docker run servier servier predict --smile <args>
>>>>>>> 9f56777d88be66a417cf5987b2b556f4d073e169
