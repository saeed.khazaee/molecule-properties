from setuptools import setup, find_packages

setup(
    name='servier',
    version='2023.1.0',
    packages=find_packages(),
    install_requires=[
        'tensorflow',
        'scikit-learn',
        'matplotlib',
        'deepchem',
        'rdkit',
        'pandas',
        'requests',
        'numpy'
        
    ],
    entry_points={
        'console_scripts': [
            'servier=servier.main:main',
        ],
    },
)
