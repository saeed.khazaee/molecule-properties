import pandas as pd
from sklearn.model_selection import train_test_split
from .augmentation import augment_smiles

def load_data(file_path):
    return pd.read_csv(file_path)

def preprocess_data(df):
    X_train_val, X_test, y_train_val, y_test = train_test_split(df['smiles'], df['P1'], test_size=0.3, random_state=42)
    X_train, X_val, y_train, y_val = train_test_split(X_train_val, y_train_val, test_size=0.2, random_state=42)

    X_train_augmented = []
    y_train_augmented = []

    for smile, label in zip(X_train, y_train):
        num_augmentations = 16 if label == 0 else 4
        augmented_smiles = augment_smiles(smile, num_augmentations)
        X_train_augmented.extend(augmented_smiles)
        y_train_augmented.extend([label] * num_augmentations)

    X_train = X_train_augmented
    y_train = y_train_augmented

    return X_train, X_val, X_test, y_train, y_val, y_test
