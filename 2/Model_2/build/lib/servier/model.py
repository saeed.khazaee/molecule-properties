import numpy as np
import deepchem as dc
from deepchem.models import GraphConvModel
from rdkit import Chem
import os
class DeepChemModel():
    def __init__(self, model_path,train_dataset=None, val_dataset=None, test_dataset=None,  n_tasks=1, mode='classification', dropout=0.2, learning_rate=0.005, batch_size=100):


        self.train_dataset = train_dataset
        self.val_dataset = val_dataset
        self.test_dataset = test_dataset
        self.model = GraphConvModel(n_tasks=1, mode='classification', dropout=0.2,
    learning_rate=0.005,
        batch_size=100,model_dir=model_path)
        self.model_path=model_path
    def train_model(self, nb_epoch=50):
        # Train the model

        self.model.fit(self.train_dataset, nb_epoch=50)
        print("The model has been trained!")


    # def predict_smiles(self, smiles_string):
    #     # Convert SMILES string to RDKit Mol object
    #     mol = Chem.MolFromSmiles(smiles_string)
    
    #     # Convert mol to desired features 
    #     featurizer = dc.feat.ConvMolFeaturizer()
    #     features = featurizer.featurize([mol])
    
    #     # Predict using the model
    #     prediction = np.argmax(self.model.predict(features), axis=1)
    def predict_smiles(self,smiles_string):
        # Load the model
       
        _,model=self.load_from_dir(self.model_path)
            # Convert SMILES string to RDKit Mol object
        

            
        # Create a dataset from the SMILES string
        smiles = [smiles_string]
        mols = [Chem.MolFromSmiles(s) for s in smiles]
        featurizer = dc.feat.ConvMolFeaturizer()
        x = featurizer.featurize(mols)
    
        # Make predictions
        predictions = np.argmax(model.predict_on_batch(x))
        return predictions

      

    def evaluate_model(self,test_dataset,metrics):
        _,model=self.load_from_dir(self.model_path)
        test_scores = model.evaluate_model(test_dataset,metrics)
        if test_scores is not None:
            for metric_name, metric_value in test_scores.items():
                print(f'{metric_name}: {metric_value}')
        else:
            print("Failed to get test scores.")

            

    def load_from_dir(self, model_path):
        # Restore the weights from the checkpoint
        self.model.restore(os.path.join(model_path, "ckpt-2"))
        # Return the current model instance
        return self,self.model

