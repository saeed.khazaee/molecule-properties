import argparse
from servier.user_command import train, evaluate, predict

def main():
    parser = argparse.ArgumentParser(description='Servier Command Line Interface')
    subparsers = parser.add_subparsers(dest='command', help='Available commands')

    # Train command
    train_parser = subparsers.add_parser('train', help='Train the model')
    train_parser.add_argument('--dataset_path', type=str, help='Path to the dataset', required=True)
    train_parser.add_argument('--model_path', type=str, help='Path to save the trained model', required=True)
    train_parser.set_defaults(func=train)

    # Evaluate command
    eval_parser = subparsers.add_parser('evaluate', help='Evaluate the model')
    eval_parser.add_argument('--dataset_path', type=str, help='Path to the dataset', required=True)
    eval_parser.add_argument('--model_path', type=str, help='Path to the trained model', required=True)
    eval_parser.set_defaults(func=evaluate)

    # Predict command
    predict_parser = subparsers.add_parser('predict', help='Predict property for a given smile')
    predict_parser.add_argument('--smile', type=str, help='Smile string', required=True)
    predict_parser.add_argument('--model_path', type=str, help='Path to the trained model', required=True)
    predict_parser.set_defaults(func=predict)

    args = parser.parse_args()
    args.func(args)

if __name__ == '__main__':
    main()
