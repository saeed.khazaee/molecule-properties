import deepchem as dc
import numpy as np
def compute_metrics():
    # ROC-AUC
    roc_auc_metric = dc.metrics.Metric(dc.metrics.roc_auc_score, np.mean)
    
    # Accuracy
    accuracy_metric = dc.metrics.Metric(dc.metrics.accuracy_score)
    f1_metric = dc.metrics.Metric(dc.metrics.f1_score)
    # Precision
    # precision_metric = dc.metrics.Metric(dc.metrics.precision_score, mode="classification")
    
    # Recall
    recall_metric = dc.metrics.Metric(dc.metrics.recall_score, mode="classification")
    
    
    metrics = [roc_auc_metric, accuracy_metric, f1_metric, recall_metric]
    
    
    return metrics
