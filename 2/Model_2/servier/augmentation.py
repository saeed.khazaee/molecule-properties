from rdkit.Chem import MolFromSmiles, MolToSmiles
import random

def augment_smiles(smiles, num_augmentations):
    if num_augmentations == 1:
        return [smiles]
    mol = MolFromSmiles(smiles)
    augmented_smiles = [MolToSmiles(mol, doRandom=True) for _ in range(num_augmentations)]
    return augmented_smiles
