from servier.model import DeepChemModel
from servier.preprocessing import load_data, split_data, augment_data,data_to_deepchem
from servier.metrics import compute_metrics
import numpy as np
import pandas as pd



def train(args):

    train_dataset, val_dataset, test_dataset=pass_data(args.dataset_path)
    
    # Train the model
    model=DeepChemModel(args.model_path,train_dataset, val_dataset, test_dataset)
    model.train_model()

    

def evaluate(args):

    
    train_dataset, val_dataset, test_dataset=pass_data(args.dataset_path)
    # Initialize the model
    model=DeepChemModel(args.model_path,train_dataset, val_dataset, test_dataset)
    # Load the trained model
    model = model.load_from_dir(args.model_path)
    # Compute metrics
    metrics = compute_metrics()
    test_scores = model.evaluate_model(test_dataset,metrics)
    for metric_name, metric_value in test_scores.items():
        print(f'{metric_name}: {metric_value}')



def predict(args):
    # train_dataset, val_dataset, test_dataset=pass_data(args.dataset_path)
    # Load the trained model
    model=DeepChemModel(args.model_path)
    model,_ = model.load_from_dir(args.model_path)
    
    # Make predictions for a given smile
    predicted = model.predict_smiles(args.smile)
    
    # Print the prediction
    print('Prediction for Smile:', args.smile)
    print('P1:', predicted)
def pass_data(path):
    df = load_data(path)
    df_train_val, df_test, df_train, df_val = split_data(df)
    df_train = augment_data(df_train)
    train_dataset = data_to_deepchem(df_train)
    val_dataset = data_to_deepchem(df_val)
    test_dataset = data_to_deepchem(df_test)
    return train_dataset, val_dataset, test_dataset