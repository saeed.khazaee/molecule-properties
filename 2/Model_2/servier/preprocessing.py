# preprocessing
from sklearn.model_selection import train_test_split
import pandas as pd
import numpy as np
from servier.augmentation import augment_smiles
from deepchem.feat import ConvMolFeaturizer
import deepchem as dc


def load_data(csv_path):
    return pd.read_csv(csv_path)

def split_data(df):
    df_train_val, df_test = train_test_split(df, test_size=0.2, random_state=42)
    df_train, df_val = train_test_split(df_train_val, test_size=0.2, random_state=42)
    return df_train_val, df_test, df_train, df_val

def augment_data(df):
    X_train_augmented = []
    y_train_augmented = []

    for smile, label in zip(df['smiles'], df['P1']):
        num_augmentations = 1 if label == 0 else 1
        augmented_smiles = augment_smiles(smile, num_augmentations)
        X_train_augmented.extend(augmented_smiles)
        y_train_augmented.extend([label] * num_augmentations)

    train_dict = {'smiles': X_train_augmented,'P1': y_train_augmented}
    df_augmented = pd.DataFrame(train_dict)
    
    return df_augmented


def data_to_deepchem(df):
    smiles = df['smiles'].values
    P1 = df['P1'].values
    # Compute the number of examples in each class
    class_counts = np.bincount(df['P1'].values.astype(int))
    
    # Compute class weights
    class_weights = 1. / class_counts
    # Calculate weights for each sample in the dataset
    sample_weights = class_weights[df['P1'].values.astype(int)]
    
    featurizer = ConvMolFeaturizer()
    features = featurizer.featurize(smiles)

    dataset = dc.data.NumpyDataset(X=features, y=P1,w=sample_weights)

    return dataset