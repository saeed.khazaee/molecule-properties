from setuptools import setup, find_packages

setup(
    name='servier',
    version='2023.2.0',
    description='Command Line Interface for Training, Evaluating and Predicting using DeepChem Model',
    
    packages=find_packages(),
    install_requires=[
        'numpy',
        'pandas',
        'argparse',
        'deepchem'
        
    ],
    entry_points={
        'console_scripts': [
            'servier=servier.main:main',
        ],
    },
)

