from sklearn.metrics import roc_auc_score, accuracy_score, precision_score, recall_score,hamming_loss,classification_report


def compute_metrics(y_true, y_pred):
    accuracy=accuracy_score(y_true, y_pred)
    precision = precision_score(y_true, y_pred,average='samples')
    recall = recall_score(y_true, y_pred,average='samples')
    auc = roc_auc_score(y_true, y_pred)
    
    hamming=hamming_loss(y_true, y_pred)
    
    report=classification_report(y_true, y_pred)
    return accuracy, precision, recall, auc,hamming,report
