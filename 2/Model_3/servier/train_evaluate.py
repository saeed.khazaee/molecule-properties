import numpy as np
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.losses import BinaryCrossentropy, MeanSquaredError
from tensorflow.keras.callbacks import EarlyStopping
from tensorflow.keras import Model, Input
from tensorflow.keras.layers import Dense, Dropout
from keras.wrappers.scikit_learn import KerasClassifier
from servier.model import DNNModel, Autoencoder
from servier.metrics import compute_metrics
from servier.feature_extractor import FeatureExtractor
from servier.data_preprocessing import load_data, preprocess_data
from sklearn.model_selection import RandomizedSearchCV

fe=FeatureExtractor()

def train_model(X_train, y_train, X_val, y_val):
    # Train the model
    input_dim_ae = 2048
    hidden_dim_ae = 500
    hidden_dim_dnn = 500
    output_dim = 9

    # Train the autoencoder
    autoencoder = Autoencoder(input_dim_ae, hidden_dim_ae)
    autoencoder.autoencoder.compile(optimizer=Adam(0.001), loss=MeanSquaredError())
    autoencoder.autoencoder.fit(X_train, X_train, epochs=50, batch_size=32)

    # Rebuild and copy weights to the encoder
    encoder_input = Input(shape=(input_dim_ae,))
    encoder_output = Dense(hidden_dim_ae, activation='sigmoid')(encoder_input)
    encoder = Model(encoder_input, encoder_output)
    encoder.set_weights(autoencoder.encoder.get_weights())

    # Extract encoded features using the trained autoencoder
    encoded_X_train = encoder.predict(X_train)
    encoded_X_val = encoder.predict(X_val)

    # # Train the DNN model using the encoded features
    # dnn_model = DNNModel(hidden_dim_ae, hidden_dim_dnn, output_dim)
    # dnn_model.model.compile(optimizer=Adam(0.001), loss=BinaryCrossentropy())
    # early_stopping = EarlyStopping(patience=10)
    # dnn_model.model.fit(encoded_X_train, y_train, epochs=50, batch_size=32, validation_data=(encoded_X_val, y_val),
    #                     callbacks=[early_stopping])
    # return dnn_model, encoder

    # Define model
    dnn_model = KerasClassifier(build_fn=DNNModel.build_fn, epochs=50, batch_size=32, verbose=0)

    # Define hyperparameter search space
    param_grid = {
        'input_dim': [hidden_dim_ae],
        'hidden_dim': [hidden_dim_dnn],
        'output_dim': [output_dim],
        'dropout_rate': [0.1, 0.2, 0.3, 0.4],
        'l2_reg': [0.01, 0.1, 1.0]
    }

    # Perform random search
    random_search = RandomizedSearchCV(estimator=dnn_model, param_distributions=param_grid, n_iter=10, cv=3)
    random_search_result = random_search.fit(encoded_X_train, y_train)

    # Get the best model and its hyperparameters
    best_model = random_search_result.best_estimator_.model
    best_hyperparameters = random_search_result.best_params_
    
    # Calculate class weights
    class_totals = y_train.sum(axis=0)
    class_weights = class_totals.max() / class_totals
    class_weights = {i: weight for i, weight in enumerate(class_weights)}
    
    # Train the final DNN model using the best hyperparameters
    best_dnn_model = DNNModel(hidden_dim_ae, hidden_dim_dnn, output_dim,
                              dropout_rate=best_hyperparameters['dropout_rate'],
                              l2_reg=best_hyperparameters['l2_reg'])
    
    best_dnn_model.model.compile(optimizer=Adam(0.001), loss=BinaryCrossentropy())
    early_stopping = EarlyStopping(patience=10)
    best_dnn_model.model.fit(encoded_X_train, y_train, epochs=50, batch_size=32,
                             validation_data=(encoded_X_val, y_val), callbacks=[early_stopping],
                             class_weight=class_weights)


    return best_dnn_model, encoder
def evaluate_model(dnn_model, autoencoder_model, X_test, y_test):
    encoded_X_test = autoencoder_model.predict(X_test)
    print("==============",encoded_X_test.shape)
    test_outputs = dnn_model.predict(encoded_X_test)
    predicted = (test_outputs > 0.5).astype(float)
    accuracy, precision, recall, auc,hamming,report = compute_metrics(y_test, predicted)
    return accuracy, precision, recall, auc,hamming,report
def predict_property(smile, dnn_model,autoencoder):
    fingerprint = np.array([fe.fingerprint_features(smile)])
    encoded_fingerprint = autoencoder.predict(fingerprint)
    prediction = dnn_model.predict(encoded_fingerprint)[0]
    return prediction

