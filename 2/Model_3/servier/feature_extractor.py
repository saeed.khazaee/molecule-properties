from rdkit.Chem import rdMolDescriptors, MolFromSmiles, rdmolfiles, rdmolops
import numpy as np

class FeatureExtractor:
    def fingerprint_features(self, smile_string, radius=2, size=2048):
        mol_to_graph = MolFromSmiles(smile_string)
        new_order = rdmolfiles.CanonicalRankAtoms(mol_to_graph)
        mol = rdmolops.RenumberAtoms(mol_to_graph, new_order)
        fingerprint = rdMolDescriptors.GetMorganFingerprintAsBitVect(
            mol, radius, nBits=size, useChirality=True, useBondTypes=True, useFeatures=False
        )
        return np.array(fingerprint, dtype=np.float32)
