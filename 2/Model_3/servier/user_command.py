from servier.model import DNNModel, Autoencoder
from servier.metrics import compute_metrics
from servier.feature_extractor import FeatureExtractor
from servier.data_preprocessing import load_data, preprocess_data
from servier.train_evaluate import train_model, evaluate_model, predict_property
from tensorflow.keras.models import load_model
import numpy as np
fe=FeatureExtractor()
def train(args):
    # Load and preprocess the data
    df = load_data(args.dataset_path)
    X_train, X_val, X_test, y_train, y_val, y_test = preprocess_data(df)
    # Load and preprocess the data
    df = load_data(args.dataset_path)
    X_train, X_val, X_test, y_train, y_val, y_test = preprocess_data(df)
    X_train, X_val, X_test, y_train, y_val, y_test=refine_data(X_train, X_val, X_test, y_train, y_val, y_test)
    print("preprocessing has been done....")
    # Train the model
    dnn_model, autoencoder = train_model(X_train, y_train, X_val, y_val)

    # Save the trained model
    dnn_model.model.save(args.model_path)
    autoencoder.save(args.autoencoder_path)

def evaluate(args):

    # Load the trained model

    dnn_model = load_model(args.model_path)
    autoencoder = load_model(args.autoencoder_path)
    
    # Load and preprocess the data
    df = load_data(args.dataset_path)
    X_train, X_val, X_test, y_train, y_val, y_test = preprocess_data(df)
    X_train, X_val, X_test, y_train, y_val, y_test=refine_data(X_train, X_val, X_test, y_train, y_val, y_test)
    accuracy, precision, recall, auc,hamming,report=evaluate_model(dnn_model, autoencoder, X_test, y_test)


    # Print evaluation metrics
    print('hamming loss :%.3f' % hamming)
    print('Accuracy: %.3f' % accuracy)
    print('Precision: %.3f' % precision)
    print('Recall: %.3f' % recall)
    print('AUC: %.3f' % auc)
    print('Classification report:', report)

def predict(args):

    
    dnn_model = load_model(args.model_path)
    autoencoder = load_model(args.autoencoder_path)

    # Make predictions for a given smile
    prediction = predict_property(args.smile, dnn_model,autoencoder)

    # Print the prediction
    print('Prediction for Smile:', args.smile)
    print('P1:', prediction)
def refine_data(X_train, X_val, X_test, y_train, y_val, y_test):
    smiles_for_train = X_train
    X_train = np.array([fe.fingerprint_features(smile) for smile in smiles_for_train])
    y_train = np.array(y_train)
    
    smiles_for_val = X_val
    X_val = np.array([fe.fingerprint_features(smile) for smile in smiles_for_val])
    y_val = np.array(y_val)
    
    smiles_for_test = X_test
    X_test = np.array([fe.fingerprint_features(smile) for smile in smiles_for_test])
    y_test = np.array(y_test)
    return X_train, X_val, X_test, y_train, y_val, y_test