import tensorflow as tf
from tensorflow.keras import Model, Input
from tensorflow.keras.layers import Dense, Dropout
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.losses import BinaryCrossentropy, MeanSquaredError
from tensorflow.keras.callbacks import EarlyStopping
from tensorflow.keras import regularizers
class DNNModel:
    # def __init__(self, input_dim, hidden_dim, output_dim):
    #     self.input_dim = input_dim
    #     self.hidden_dim = hidden_dim
    #     self.output_dim =output_dim
    #     self.model = self.build_model()

    # def build_model(self):
    #     inputs = Input(shape=(self.input_dim,))
    #     x = Dense(self.hidden_dim, activation='relu')(inputs)
    #     x = Dropout(0.2)(x)
    #     x = Dense(self.hidden_dim, activation='relu')(x)
    #     x = Dropout(0.2)(x)
    #     outputs = Dense(self.output_dim, activation='sigmoid')(x)
    #     model = Model(inputs, outputs)
    #     return model
    def __init__(self, input_dim, hidden_dim, output_dim, dropout_rate=0.2, l2_reg=0.01):
        self.input_dim = input_dim
        self.hidden_dim = hidden_dim
        self.output_dim = output_dim
        self.dropout_rate = dropout_rate
        self.l2_reg = l2_reg
        self.model = self.build_model()

    def build_model(self):
        inputs = Input(shape=(self.input_dim,))
        x = Dense(self.hidden_dim, activation='relu', kernel_regularizer=regularizers.l2(self.l2_reg))(inputs)
        x = Dropout(self.dropout_rate)(x)
        x = Dense(self.hidden_dim, activation='relu', kernel_regularizer=regularizers.l2(self.l2_reg))(x)
        x = Dropout(self.dropout_rate)(x)
        outputs = Dense(self.output_dim, activation='sigmoid')(x)
        model = Model(inputs, outputs)
        model.compile(optimizer=Adam(0.001), loss=BinaryCrossentropy(), metrics=['accuracy'])
        return model

    @staticmethod
    def build_fn(input_dim, hidden_dim, output_dim, dropout_rate=0.2, l2_reg=0.01):
        model = DNNModel(input_dim, hidden_dim, output_dim, dropout_rate, l2_reg)
        return model.model
class Autoencoder:
    def __init__(self, input_dim, hidden_dim):
        self.input_dim = input_dim
        self.hidden_dim = hidden_dim
        self.autoencoder, self.encoder = self.build_autoencoder()

    def build_autoencoder(self):
        inputs = Input(shape=(self.input_dim,))
        encoded = Dense(self.hidden_dim, activation='sigmoid')(inputs)
        decoded = Dense(self.input_dim, activation='sigmoid')(encoded)
        autoencoder = Model(inputs, decoded)
        encoder = Model(inputs, encoded)
        
        return autoencoder, encoder

    def train(self, X_train):
        self.autoencoder.compile(optimizer='adam', loss='binary_crossentropy')
        self.autoencoder.fit(X_train, X_train, epochs=100, batch_size=64, verbose=1)

    def encode(self, X):
        return self.encoder.predict(X)
