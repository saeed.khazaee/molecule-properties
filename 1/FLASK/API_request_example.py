# request
import requests
import json

# Define the URL
url = "http://localhost:5000/predict"

# Define the header
headers = {"Content-Type": "application/json"}

# Define the data you want to send
data = {
    "smile": "OCC3OC(OCC2OCC(O)C(O)C2O)C(O)C(O)C3O"  
}

# Send the POST request
response = requests.post(url, headers=headers, data=json.dumps(data))

# Print the response
print(response.json())