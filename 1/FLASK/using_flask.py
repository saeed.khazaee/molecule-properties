from flask import Flask, jsonify, request, render_template
import numpy as np
from tensorflow.keras.models import load_model
from feature_extractor import FeatureExtractor


app = Flask(__name__)

# Load the trained model
model = load_model('dnn.h5')
encoder=load_model('en.h5')
fe = FeatureExtractor()  # Assuming you have the FeatureExtractor class implemented

@app.route('/', methods=['GET'])
def home():
    return render_template('predict_form.html')

@app.route('/predict', methods=['POST'])
def predict():
    data = request.json  # Get the request data as JSON
    print(f"Received data: {data}")  # Debug print

    smile = data['smile']  # Extract the SMILE string from the request data
    print(f"Smile: {smile}")  # Debug print

    # Preprocess the input molecule
    fingerprint = np.array([fe.fingerprint_features(smile)])
    print(f"Fingerprint: {fingerprint}")  # Debug print

    encoded=encoder.predict(fingerprint)
    print(f"Encoded: {encoded}")  # Debug print

    # Make the prediction using the loaded model
    prediction = model.predict(encoded)[0]
    print(f"Prediction: {prediction}")  # Debug print
    
    prediction=1 if prediction >0.5 else 0
    
    # Prepare the response data
    response = {'prediction': float(prediction)}

    return jsonify(response)


if __name__ == '__main__':
    app.run(debug=True)
